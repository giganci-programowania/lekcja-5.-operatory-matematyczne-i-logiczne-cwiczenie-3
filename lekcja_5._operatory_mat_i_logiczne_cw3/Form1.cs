﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lekcja_5._operatory_mat_i_logiczne_cw3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOblicz_Click(object sender, EventArgs e)
        {
            int wpisanaLiczba = int.Parse(txtLiczba.Text);
            
            /* Operator "++" odpowiada za inkrementacje, czyli zwiększenie liczby o 1
             * Istnieją dwa rodzaje inkrementacji: pre oraz post
             * Wyrażenie "++Zmienna" jest pre-inkrementacja, czyli najpierw następuje zwiększenie o 1, a następnie użycie wartości
             * Wyrażenie "zmienna++" jest post-inkrementacja, czyli najpierw użycie wartości, a następnie zwiększenie o 1 */
            int liczbaWieksza = wpisanaLiczba;
            ++liczbaWieksza;
            
            /* Operator "--" odpowiada dekrementacje, czyli zmniejszenie liczby o 1
             * Dekrementacja, tak samo jak inkrementacja, dzieli się na dwa rodzaje: pre i post
             * Te dwa typy działają na podobnej zasadzie co w inkrementacji */
            int liczbaMniejsza = wpisanaLiczba;
            --liczbaMniejsza;

            /* Znak specjalny "\n" jest znakiem nowej linii (Enter)
             * Pozwala on przenosić tekst do następnej linii (łamać linię) */
            MessageBox.Show("Liczba o 1 wieksza to " + liczbaWieksza.ToString()
                            + "\nLiczba o 1 mniejsza to " + liczbaMniejsza.ToString(), "Wynik", MessageBoxButtons.OK);
        }
    }
}
